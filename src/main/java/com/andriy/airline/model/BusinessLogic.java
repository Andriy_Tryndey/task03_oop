package com.andriy.airline.model;

import java.util.List;

/**  business logic with implemented methods
 *   the findAllCargoPlanes () method finds all cargo planes
 *   the findAllPassengerPlanets () method finds all passenger planes
 *   findPassengerByPassengerCapacity (int numberOfSeats) method finds a plane for a certain number of passengers
 *   findCargoPlanesByCarringCapacity (int carryingCapacity) method finds an airplane for a given load weight
 */
public class BusinessLogic implements Model {
    private Airline airline;

    public BusinessLogic() {
        airline = new Airline();
    }

    @Override
    public List<CargoPlane> findAllCargoPlanes() {
        return airline.getCargoPlanes();
    }

    @Override
    public List<PassengerPlane> findAllPassengerPlanets() {
        return airline.getPassengerPlanes();
    }

    @Override
    public PassengerPlane findPassengerByFuelConsumption(int fuelConsumption) {
        PassengerPlane pass = null;
        List<PassengerPlane> passengerPlanes = airline.getPassengerPlanes();
        for (PassengerPlane passengerPl : passengerPlanes) {
            if (passengerPl.getFuelConsumption() >= fuelConsumption)
            pass = passengerPl;
        }
        return pass;
    }

    @Override
    public CargoPlane findCargoPlanesByFuelConsumption(int fuelConsumption) {
        CargoPlane carg = null;
        List<CargoPlane> cargoPlanes = airline.getCargoPlanes();
        for (CargoPlane cargoPl : cargoPlanes) {
            if (cargoPl.getFuelConsumption() >= fuelConsumption){
                carg = cargoPl;
            }
        }
        return carg;
    }


}
