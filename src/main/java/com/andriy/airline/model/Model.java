package com.andriy.airline.model;

import java.util.List;

public interface Model {
    List<CargoPlane> findAllCargoPlanes();
    List<PassengerPlane> findAllPassengerPlanets();
    PassengerPlane findPassengerByFuelConsumption(int fuelConsumption);
    CargoPlane findCargoPlanesByFuelConsumption(int fuelConsumption);
}
