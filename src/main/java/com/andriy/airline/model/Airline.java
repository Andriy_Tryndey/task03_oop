package com.andriy.airline.model;

import java.util.LinkedList;
import java.util.List;

/**
 * class to create an airport
 */
public class Airline {
    private CargoPlane cargoPlane;
    private PassengerPlane passengerPlane;
    private List<CargoPlane> cargoPlanes;
    private List<PassengerPlane> passengerPlanes;

    public Airline() {
        super();
        cargoPlanes = new LinkedList<>();
        passengerPlanes = new LinkedList<>();
        generationCargoPlanes();
        generationPassengerPlanes();
    }
    public void generationCargoPlanes(){
        cargoPlanes.add(new CargoPlane("Gruz1", "Yellow", 10000, 100,20));
        cargoPlanes.add(new CargoPlane("Gruz2", "Green", 8000, 150, 50));
        cargoPlanes.add(new CargoPlane("Gruz3", "Red", 15000, 200, 100));
        cargoPlanes.add(new CargoPlane("Gruz4", "Blue", 12000, 150, 30));
        cargoPlanes.add(new CargoPlane("Gruz5", "Yellow", 50000, 200, 80));
    }
    public void generationPassengerPlanes(){
        passengerPlanes.add(new PassengerPlane("Pass1", "White", 30000, 150, 200));
        passengerPlanes.add(new PassengerPlane("Pass2", "White", 30000, 150, 200));
        passengerPlanes.add(new PassengerPlane("Pass3", "White", 50000, 100, 400));
        passengerPlanes.add(new PassengerPlane("Pass4", "White", 50000, 100, 400));
        passengerPlanes.add(new PassengerPlane("Pass5", "Green", 100000, 100, 300));
        passengerPlanes.add(new PassengerPlane("Pass6", "Green", 100000, 100, 300));
        passengerPlanes.add(new PassengerPlane("Pass7", "Green", 80000, 100, 500));
    }

    public CargoPlane getCargoPlane() {
        return cargoPlane;
    }

    public Airline setCargoPlane(CargoPlane cargoPlane) {
        this.cargoPlane = cargoPlane;
        return this;
    }

    public PassengerPlane getPassengerPlane() {
        return passengerPlane;
    }

    public Airline setPassengerPlane(PassengerPlane passengerPlane) {
        this.passengerPlane = passengerPlane;
        return this;
    }

    public List<CargoPlane> getCargoPlanes() {
        return cargoPlanes;
    }

    public Airline setCargoPlanes(List<CargoPlane> cargoPlanes) {
        this.cargoPlanes = cargoPlanes;
        return this;
    }

    public List<PassengerPlane> getPassengerPlanes() {
        return passengerPlanes;
    }

    public void setPassengerPlanes(List<PassengerPlane> passengerPlanes) {
        this.passengerPlanes = passengerPlanes;
    }
}
