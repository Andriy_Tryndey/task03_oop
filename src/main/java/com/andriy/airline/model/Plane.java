package com.andriy.airline.model;

/**  parent class
 *   with general parameters and methods
 */
public abstract class Plane {
    private String model;
    private String color;
    private int flightRange;
    private int fuelConsumption;

    public Plane() {
    }

    public Plane(String model, String color, int flightRange, int fuelConsumption) {
        this.model = model;
        this.color = color;
        this.flightRange = flightRange;
        this.fuelConsumption = fuelConsumption;
    }
    public abstract int price();

    public String getModel() {
        return model;
    }

    public Plane setModel(String model) {
        this.model = model;
        return this;
    }

    public String getColor() {
        return color;
    }

    public Plane setColor(String color) {
        this.color = color;
        return this;
    }

    public int getFlightRange() {
        return flightRange;
    }

    public Plane setFlightRange(int flightRange) {
        this.flightRange = flightRange;
        return this;
    }

    public int getFuelConsumption() {
        return fuelConsumption;
    }

    public Plane setFuelConsumption(int fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
        return this;
    }

    @Override
    public String toString() {
        return "Plane{" +
                "model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", flightRange=" + flightRange +
                ", fuelConsumption=" + fuelConsumption +
                '}';
    }
}
