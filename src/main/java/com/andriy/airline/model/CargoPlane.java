package com.andriy.airline.model;

/**
 *  model for creating cargo planes
 */
public class CargoPlane extends Plane {
    private int carryingCapacity;

    public CargoPlane(String model, String color, int flightRange, int fuelConsumption, int carryingCapacity) {
        super(model, color, flightRange, fuelConsumption);
        this.carryingCapacity = carryingCapacity;
    }

    public int price() {
        return 1000;
    }

    public int getCarryingCapacity() {
        return carryingCapacity;
    }

    public CargoPlane setCarryingCapacity(int carryingCapacity) {
        this.carryingCapacity = carryingCapacity;
        return this;
    }

    @Override
    public String toString() {
        return "CargoPlane{" + super.toString() +
                "carryingCapacity=" + carryingCapacity +
                '}';
    }
}
