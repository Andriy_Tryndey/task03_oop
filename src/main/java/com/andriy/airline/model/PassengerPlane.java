package com.andriy.airline.model;

/**
 * model for creating passenger planes
 */
public class PassengerPlane extends Plane {
    private int numberOfSeats;

    public PassengerPlane(String model, String color, int flightRange, int fuelConsumption, int numberOfSeats) {
        super(model, color, flightRange, fuelConsumption);
        this.numberOfSeats = numberOfSeats;
    }

    public int price() {
        return 200;

    }
    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public PassengerPlane setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
        return this;
    }

    @Override
    public String toString() {
        return "PassengerPlane{" + super.toString() +
                "numberOfSeats=" + numberOfSeats +
                '}';
    }
}
