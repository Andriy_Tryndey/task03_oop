package com.andriy.airline.controller.passenger;

import com.andriy.airline.model.BusinessLogic;
import com.andriy.airline.model.Model;
import com.andriy.airline.model.PassengerPlane;

import java.util.List;

public class PassengerControllerImpl implements PassengerController {
    private Model model;
    private List<PassengerPlane> passengerPlanes;

    public PassengerControllerImpl(){
        model = new BusinessLogic();
    }

    @Override
    public List<PassengerPlane> findPassengers() {
        return model.findAllPassengerPlanets();
    }

    @Override
    public PassengerPlane findPassengerByFuelConsumption(int fuelConsumption) {
        PassengerPlane passengerPlane;
        passengerPlane = model.findPassengerByFuelConsumption(fuelConsumption);
        return passengerPlane;
    }
}
