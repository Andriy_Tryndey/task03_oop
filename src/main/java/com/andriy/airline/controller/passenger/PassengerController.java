package com.andriy.airline.controller.passenger;

import com.andriy.airline.model.PassengerPlane;

import java.util.List;

public interface PassengerController {
    List<PassengerPlane> findPassengers();
    PassengerPlane findPassengerByFuelConsumption(int fuelConsumption);
}
