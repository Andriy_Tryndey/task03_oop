package com.andriy.airline.controller.cargo;

import com.andriy.airline.model.CargoPlane;

import java.util.List;

public interface CargoController {
    List<CargoPlane> findCargoPlanes();
    CargoPlane findCargoPlanesByFuelConsumption(int fuelConsumptionc);
}
