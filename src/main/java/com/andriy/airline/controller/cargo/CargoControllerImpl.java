package com.andriy.airline.controller.cargo;

import com.andriy.airline.controller.cargo.CargoController;
import com.andriy.airline.model.*;

import java.util.List;

public class CargoControllerImpl implements CargoController {
    private Model model;
    private List<CargoPlane> cargoPlanes;

    public CargoControllerImpl(){
        model = new BusinessLogic();
    }
    @Override
    public List<CargoPlane> findCargoPlanes() {
        return model.findAllCargoPlanes();
    }

    @Override
    public CargoPlane findCargoPlanesByFuelConsumption(int fuelConsumption) {
        CargoPlane cargoPlane;
        cargoPlane = model.findCargoPlanesByFuelConsumption(fuelConsumption);
        return cargoPlane;
    }

}
