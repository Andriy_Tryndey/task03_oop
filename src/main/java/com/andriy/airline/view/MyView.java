package com.andriy.airline.view;

import com.andriy.airline.controller.cargo.CargoControllerImpl;
import com.andriy.airline.controller.passenger.PassengerControllerImpl;
import com.andriy.airline.model.CargoPlane;
import com.andriy.airline.model.PassengerPlane;

import java.util.*;

/**
 * display of information on the screen
 */
public class MyView {
    private CargoControllerImpl cargoController;
    private PassengerControllerImpl passengerController;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner sc = new Scanner(System.in);

    public MyView() {
        cargoController = new CargoControllerImpl();
        passengerController = new PassengerControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "1 = print CargoPlaneList");
        menu.put("2", "2 = print PassengerPlaneList");
        menu.put("3", "3 = print General places fo passenger");
        menu.put("4", "4 = print General carrying capacity");
        menu.put("5", "5 = print sort planes by flight range");
        menu.put("6", "6 = print planes that meets the specified fuel consumption parameter range");
        menu.put("Q", "Q = exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
    }

    private void pressButton1() {
        System.out.println(cargoController.findCargoPlanes());
    }

    private void pressButton2() {
        System.out.println(passengerController.findPassengers());
    }

    private void pressButton3() {
        int seats = 0;
        List<PassengerPlane> passengers = passengerController.findPassengers();
        for (PassengerPlane passenger : passengers) {
            seats = seats + passenger.getNumberOfSeats();
        }
        System.out.println("General places fo passenger = " + seats);
    }

    private void pressButton4() {
        int carringCapacity = 0;
        List<CargoPlane> cargoPlanes = cargoController.findCargoPlanes();
        for (CargoPlane cargoPlane : cargoPlanes) {
            carringCapacity = carringCapacity + cargoPlane.getCarryingCapacity();
        }
        System.out.println("General carrying capacity = " + carringCapacity);
    }

    private void pressButton5() {

    }

    private void pressButton6() {
        System.out.println("enter fuel consumption");
        int fuel = sc.nextInt();
        CargoPlane cargoPlanesByFuelConsumption = cargoController.findCargoPlanesByFuelConsumption(fuel);
        PassengerPlane passengerByFuelConsumption = passengerController.findPassengerByFuelConsumption(fuel);
        System.out.println("cargoPlanesByFuelConsumption = " + cargoPlanesByFuelConsumption  + "\n" + "passengerByFuelConsumption = " + passengerByFuelConsumption);
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {

            }
        }while (!keyMenu.equals("Q")) ;
    }
}
