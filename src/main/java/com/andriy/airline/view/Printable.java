package com.andriy.airline.view;

public interface Printable {
   void print();
}
