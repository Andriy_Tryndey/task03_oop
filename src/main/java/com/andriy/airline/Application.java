package com.andriy.airline;

import com.andriy.airline.model.Airline;
import com.andriy.airline.model.CargoPlane;
import com.andriy.airline.view.MyView;

/**
 * application startup class
 */
public class Application {
    public static void main(String[] args) {
        new MyView().show();
    }
}
